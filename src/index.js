import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

/* Renderiza el componente App en el elemento 
con id 'root' del index.html. */
ReactDOM.render(
    <App />,
    document.getElementById('root')
);
