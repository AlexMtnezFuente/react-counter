import React from 'react';

class App extends React.Component {

    constructor(props) {
        super(props);
        /* Inicializamos el estado del componente en el constructor. */
        this.state = {value: 0};
    }

    handleIncrement() {
        /* Actualizamos el estado -> Se vuelve a renderizar. */
        this.setState({value: this.state.value+1});
    }

    handleDecrement() {
        /* Actualizamos el estado -> Se vuelve a renderizar. */
        this.setState({value: this.state.value-1});
    }

    handleReset() {
        /* Actualizamos el estado -> Se vuelve a renderizar. */
        this.setState({value: 0});
    }

    /* Como App es un componente de clase, está 
    obligado a implementar el método render. */
    render() {
        return (
            <div>
                {this.state.value + ' '}
                <button onClick={() => this.handleIncrement()}>+</button>
                {' '}
                <button onClick={() => this.handleDecrement()}>-</button>
                {' '}
                <button onClick={() => this.handleReset()}>Reset</button>
            </div>
        );
    }

}

export default App;
